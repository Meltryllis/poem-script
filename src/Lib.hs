module Lib
    ( someFunc
    ) where

import           Parser
import           System.Environment

someFunc :: IO ()
someFunc = do
    x <- getArgs
    if null x then
        putStrLn "Nothing"
    else
        print $ parseTo $ unwords x ++ "\n"
