module Parser where

import           Data.List

data PathType = Zuo | You | Shang | Xia | Dao | Shun | Fan | Fen deriving (Show, Eq)
             -- 左    右     上      下    道     顺     反    分
data DefType = Shu | Hao deriving Show
            -- 数    号
data SpType = Zhi | Chu | Zhen | Wei deriving Show
           -- 之    出    真      伪
data Node = Operator (Int -> Int -> Int)
          | Path PathType
          | Number Int
          | Definer DefType
          | SpPath SpType
          | Origin
          | End
          | Output
          | Void

instance Show Node where
    show (Operator _) = "算"
    show (Path     x) = show x
    show (Number   x) = show x
    show (Definer  x) = show x
    show (SpPath   x) = show x
    show Origin       = "始"
    show End          = "终"
    show Output       = "曰"
    show Void         = " "

toNode :: Char -> Node
toNode '始' = Origin
toNode '终' = End
toNode '曰' = Output
toNode '数' = Definer Shu
toNode '号' = Definer Hao
toNode x | x `elem` pathchars = Path (fromData x paths)
         | x `elem` numchars  = Number (fromData x nums)
         | x `elem` oprtchars = Operator (fromData x oprts)
         | x `elem` spchars   = SpPath (fromData x sps)
toNode _ = Void

parseTo :: String -> [[Node]]
parseTo x = map (map toNode) (lines x)

-- helper function
findKey :: (Eq k) => k -> [(k, v)] -> Maybe v
findKey key = foldr (\(k, v) acc -> if key == k then Just v else acc) Nothing

fromJust :: Maybe a -> a
fromJust (Just a) = a
fromJust Nothing  = error "Parse Error"

fromData :: (Eq k) => k -> [(k, v)] -> v
fromData key val = fromJust $ findKey key val

-- data
pathchars :: String
pathchars = "左右上下道顺反分"

paths :: [(Char, PathType)]
paths = zip pathchars [Zuo, You, Shang, Xia, Dao, Shun, Fan, Fen]

numchars :: String
numchars = "零一二三四五六七八九十百千万"

nums :: [(Char, Int)]
nums = zip numchars ([0 .. 10] ++ [100, 1000, 10000])

oprtchars :: String
oprtchars = "加减乘除模"

oprts :: [(Char, Int -> Int -> Int)]
oprts = zip oprtchars [(+), (-), (*), div, mod]

spchars :: String
spchars = "之出真伪"

sps :: [(Char, SpType)]
sps = zip spchars [Zhi, Chu, Zhen, Wei]
