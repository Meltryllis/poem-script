module Interpreter where

data Pointer = Pin (Int, Int) (Int, Char) deriving Show
                -- (x  , y  ) (num, id  )
